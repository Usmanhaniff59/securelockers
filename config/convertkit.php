<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Default Hash Driver
    |--------------------------------------------------------------------------
    |
    | This option controls the default hash driver that will be used to hash
    | passwords for your application. By default, the bcrypt algorithm is
    | used; however, you remain free to modify this option if you wish.
    |
    | Supported: "bcrypt", "argon", "argon2id"
    |
    */

    'public_key' => env('CONVERTKIT_PUBLIC_KEY', 'lockers'),
    'secret_key' => env('CONVERTKIT_SECRET_KEY', 'lockers'),
    'list_of_forms_url' => env('CONVERTKIT_LIST_OF_FORMS', 'lockers'),
    'add_subscriber_form_url' => env('CONVERTKIT_ADD_SUBSCRIBER_TO_FORM', 'lockers'),
    'list_subscription_form_url' => env('CONVERTKIT_LIST_SUBSCRIPTION_TO_FORM', 'lockers'),
    'list_of_sequence_url' => env('CONVERTKIT_LIST_OF_SEQUENCE', 'lockers'),
    'add_subscriber_to_sequence_url' => env('CONVERTKIT_ADD_SUBSCRIBER_TO_SEQUENCE', 'lockers'),
    'list_subscription_to_sequence_url' => env('CONVERTKIT_LIST_SUBSCRIPTION_TO_SEQUENCE', 'lockers'),
    'list_of_fields_url' => env('CONVERTKIT_LIST_FIELDS', 'lockers'),
    'create_field_url' => env('CONVERTKIT_CREATE_FIELD', 'lockers'),

];
