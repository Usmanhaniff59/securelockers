<?php

namespace App\Http\Controllers;
use Response;
use Illuminate\Http\Request;


class HomeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {


        //dd($request);
        $public_key =  \Config::get('convertkit.public_key');
        $secret_key =  \Config::get('convertkit.secret_key');

        $list_of_forms_url =  \Config::get('convertkit.list_of_forms_url');
        $list_of_forms_url = str_replace("<your_public_api_key>", $public_key, "$list_of_forms_url");

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => $list_of_forms_url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_CUSTOMREQUEST => "GET",
        ));

        $response = curl_exec($curl);
        $response= json_decode($response);

//        dd($response);
        $err = curl_error($curl);
        curl_close($curl);
//        dd($response->forms);
        foreach ($response->forms as $form) {
            if($form->name == 'Pine form')
                $form_id =$form->id;
        }

       //add Subscriber
        $add_subscriber_form_url =  \Config::get('convertkit.add_subscriber_form_url');
        $add_subscriber_form_url = str_replace("<form_id>", $form_id, "$add_subscriber_form_url");
//        dd($add_subscriber_form_url);

        $curl = curl_init();
//        dd($public_key);
        $data = [
                'api_key' => $public_key,
                'first_name' => $request->name,
                'email' => $request->email,

                'fields'  =>[

                    'address' => $request->address,
                    'mobile' => $request->mobile,
                    'telephone' => $request->telephone,
                    'postcode' => $request->postcode,
                    'town' => $request->town,
                ]



        ];

        $card_postArray_json = json_encode($data);
        curl_setopt_array($curl, array(
            CURLOPT_URL => $add_subscriber_form_url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => $card_postArray_json,
            CURLOPT_HTTPHEADER => array(
                "Content-Type: application/json",
                "charset=utf-8"
            ),
        ));

        $response = curl_exec($curl);
        $response= json_decode($response);
//        dd($response);
        $err = curl_error($curl);
        curl_close($curl);

//        //list subscription
//        $list_subscription_form_url =  \Config::get('convertkit.list_subscription_form_url');
//        $list_subscription_form_url = str_replace(['<form_id>','<your_secret_api_key>'], [$form_id,$secret_key], "$list_subscription_form_url");
//
//         $curl = curl_init();
//
//        curl_setopt_array($curl, array(
//
//            CURLOPT_URL => $list_subscription_form_url,
//            CURLOPT_RETURNTRANSFER => true,
//            CURLOPT_CUSTOMREQUEST => "GET",
//
//        ));
//
//        $response = curl_exec($curl);
//        $response= json_decode($response);
////        dd($response);
//        $err = curl_error($curl);
//        curl_close($curl);


//       //list of sequences
//        $list_of_sequence =  \Config::get('convertkit.list_of_sequence_url');
//        $list_of_sequence = str_replace('<your_public_api_key>', $public_key, "$list_of_sequence");
//
//
//         $curl = curl_init();
//
//        curl_setopt_array($curl, array(
//
//            CURLOPT_URL => $list_of_sequence,
//            CURLOPT_RETURNTRANSFER => true,
//            CURLOPT_CUSTOMREQUEST => "GET",
//
//        ));
//
//        $response = curl_exec($curl);
//        $response= json_decode($response);
//
//        $err = curl_error($curl);
//        curl_close($curl);
//        foreach ($response->courses  as $sequence){
//            if($sequence->name == '1st sequence')
//                $sequence_id = $sequence->id;
//        }



//        //add subscriber to sequence
//        $add_subscriber_to_sequence_url =  \Config::get('convertkit.add_subscriber_to_sequence_url');
//        $add_subscriber_to_sequence_url = str_replace('<sequence_id>', $sequence_id, "$add_subscriber_to_sequence_url");
//
//        $curl = curl_init();
////        dd($public_key);
//        $data = [
//            'api_key' => $public_key,
//            'email' => $request->email,
//
//        ];
//
//        $card_postArray_json = json_encode($data);
//        curl_setopt_array($curl, array(
//            CURLOPT_URL => $add_subscriber_to_sequence_url,
//            CURLOPT_RETURNTRANSFER => true,
//            CURLOPT_CUSTOMREQUEST => "POST",
//            CURLOPT_POSTFIELDS => $card_postArray_json,
//            CURLOPT_HTTPHEADER => array(
//                "Content-Type: application/json",
//                "charset=utf-8"
//            ),
//        ));
//
//        $response = curl_exec($curl);
//        $response= json_decode($response);
////        dd($response);
//        $err = curl_error($curl);
//        curl_close($curl);



//        //List subscriptions to a sequence
//        $list_subscription_to_sequence =  \Config::get('convertkit.list_subscription_to_sequence');
//        $list_subscription_to_sequence = str_replace(['<sequence_id>','<your_secret_api_key>'], [$sequence_id,$secret_key], "$list_subscription_to_sequence");
//
//
//        $curl = curl_init();
//
//        curl_setopt_array($curl, array(
//
//            CURLOPT_URL => $list_subscription_to_sequence,
//            CURLOPT_RETURNTRANSFER => true,
//            CURLOPT_CUSTOMREQUEST => "GET",
//
//        ));
//
//        $response = curl_exec($curl);
//        $response= json_decode($response);
//
//        $err = curl_error($curl);
//        curl_close($curl);

//        //List subscriptions to a sequence
//        $list_of_fields_url =  \Config::get('convertkit.list_of_fields_url');
//        $list_of_fields_url = str_replace('<your_public_api_key>', $public_key, "$list_of_fields_url");
//
//       //dd($list_of_fields_url);
//
//        $curl = curl_init();
//
//        curl_setopt_array($curl, array(
//
//            CURLOPT_URL => $list_of_fields_url,
//            CURLOPT_RETURNTRANSFER => true,
//            CURLOPT_CUSTOMREQUEST => "GET",
//
//        ));
//
//        $response = curl_exec($curl);
//        $response= json_decode($response);
//         dd($response);
//        $err = curl_error($curl);
//        curl_close($curl);


//        //Create new Field
//        $create_field_url =  \Config::get('convertkit.create_field_url');
//                $curl = curl_init();
////        dd($public_key);
//        $data = [
//            'api_secret' => $secret_key,
//            'label' => ['address','telephone','mobile'],
//
//
//        ];
//
//        $data_postArray_json = json_encode($data);
//        curl_setopt_array($curl, array(
//            CURLOPT_URL => $create_field_url,
//            CURLOPT_RETURNTRANSFER => true,
//            CURLOPT_CUSTOMREQUEST => "POST",
//            CURLOPT_POSTFIELDS => $data_postArray_json,
//            CURLOPT_HTTPHEADER => array(
//                "Content-Type: application/json",
//                "charset=utf-8"
//            ),
//        ));
//
//        $response = curl_exec($curl);
//        $response= json_decode($response);
//        dd($response);
//        $err = curl_error($curl);
//        curl_close($curl);
        return redirect('/');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function brochure(){
        $filename = 'pdf_file/PRF008_Franchise-Brochure_v1.pdf';
        $path = storage_path($filename);

        return Response::make(file_get_contents($path), 200, [
            'Content-Type' => 'application/pdf',
            'Content-Disposition' => 'inline; filename="'.$filename.'"'
        ]);
    }
}
