<!DOCTYPE html>
<html lang="en">
<head>
    <title>Secure Locker Rental</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
    <link href="css/custom_style.css" rel="stylesheet">
    <script src="https://kit.fontawesome.com/c167da1aca.js" crossorigin="anonymous"></script>
</head>
<body>

<nav class="navbar navbar-default navbar-fixed-top" id="header">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a href="/" class="navbar-brand" style=""><img src="img/lockers/Secure_Logo_RGB_03.png" alt="" class="img-fluid"></a>
        </div>
        <div class="collapse navbar-collapse" id="myNavbar">
            <ul class="nav navbar-nav">
                <li class="Gotham-Black"><a href="#work" style="color: black;font-size: 14px;font-weight:bold;">HOW IT WORKS</a></li>
                <li class="Gotham-Black"><a href="#role" style="color: black;font-size: 14px;font-weight:bold;">YOUR ROLE</a></li>
                <li class="Gotham-Black"><a href="#financial" style="color: black;font-size: 14px;font-weight:bold;">FINANCIALS</a></li>
                <li class="Gotham-Black"><a href="#brochure" style="color: rgb(179, 5, 16);font-size: 14px;font-weight:bold;">REQUEST A FREE BROCHURE</a></li>
            </ul>
        </div>
    </div>
</nav>

<section id="about">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <br>
                <h6 class="ProximaNova-Black main_top_heading">
                    UNLOCK YOUR TRUE POTENTIAL.
                </h6>
                <h1 class="ProximaNova-Black main_second_heading">
                    A FRANCHISE CONCEPT UNLIKE ANY OTHER.
                </h1>
                <p class="ProximaNova-Regular main_top_para">Very few businesses offer you ‘residual income’ (that’s income that keeps coming in year after year with minimal work from you) but this one does!  This business redefines ‘part time’ to mean ‘letting the business run itself’. Sure, there’s some work to do getting things set up, but once you’re up and running this is a franchise that can be run alongside any other business or commitments with very minimal input from you. Perhaps you’re just ready to slow things down and enjoy more leisure time?</p>
                <h6 class="ProximaNova-Bold main_third_heading">Either way, this could be the perfect fit.</h6>
            </div>
            <div class="col-md-6">
                <br>
                <div class="top-img-fluid-section">
                    <img src="img/lockers/Header_Locker_v1.png" class="top-img-fluid" alt="">
                </div>
            </div>
        </div>
    </div>
</section>

<section id="work">
    <div class="container" data-aos="fade-up">
        <div class="row" >
            <div class="col-md-6 video-box" data-aos="fade-right" data-aos-delay="100">
                <h2 class="ProximaNova-Black blue work-heading">HOW DOES IT WORK?</h2>
                <p class="ProximaNova-Regular work-text">As with all great business concepts, the idea is really simple. The key to a successful business is to first identify a need, and then provide a cost-effective solution. If you can make it work for all parties then you’re onto a winner</p>
            </div>
            <div class="col-md-6  content" data-aos="fade-left" data-aos-delay="100">
                <div class="row text-right work-img-row">
                    <div class="col-md-4 col-sm-4 col-xs-4 text-center">
                        <img src="img/lockers/man-icon.png" class="img-fluid-icon" alt="">
                        <h4 class="Gotham-Black blue work-icon-text" >INSTALL</h4>
                    </div>
                    <div class="col-md-4 col-sm-4 col-xs-4 text-center">
                        <img src="img/lockers/4-StageIcons-2.png" class="img-fluid-icon"   alt="">
                        <h4 class="Gotham-Black blue work-icon-text" >RENT</h4>
                    </div>
                    <div class="col-md-4 col-sm-4 col-xs-4 text-center">
                        <img src="img/lockers/Earn.png" class="img-fluid-icon"   alt="">
                        <h4 class="Gotham-Black blue work-icon-text">EARN</h4>
                    </div>
                </div>
            </div>
        </div>

    </div>
</section>

<section id="about-video2">
    <div class="row about-row">
        <div class="col-md-6 about-col-1"></div>
        <div class="col-md-6 about-col-2">
            <div class="content-overlay about-col-2-lay">
                <div class="row about-row">
                    <div class="col-md-1"></div>
                    <div class="col-md-7 about-col-2-content">
                        <h6 class="ProximaNova-Black">THE PROBLEM? </h6>
                        <h5 class="ProximaNova-Black">HEALTH. SECURITY. BUDGET.</h5>
                        <p class="ProximaNova-Regular">
                            These days, schoolchildren and university students carry a lot of expensive equipment around. From £100+ trainers to iPhones and Laptops. All of this kit is heavy and valuable, so ideally they’d like somewhere secure to put it whilst they’re at school. Parents regularly put pressure on schools to install lockers. They want good sized, high quality lockers with decent locks. The problem is that most schools have very limited budgets and installing potentially over 1,000 high quality lockers (and maintaining them) comes at enormous cost, which they simply can’t afford.
                        </p>
                    </div>
                    <div class="col-md-4"></div>
                </div>
            </div>
        </div>
    </div>
    <div class="row about-row">
        <div class="col-md-6 about2-col-1">
            <div class="content-lay about2-col-1-lay">
                <div class="row">
                    <div class="col-md-3"></div>
                    <div class="col-md-8 about2-col-content">
                        <h6 class="ProximaNova-Black">THE SOLUTION? </h6>
                        <h5 class="ProximaNova-Black">SIMPLE. SECURE. FREE. </h5>
                        <p class="ProximaNova-Regular about2-col-content-text1">
                            At Secure Locker Rentals, we install the lockers free of charge to the school. We then make these lockers available for parents to rent on an annual basis at very low cost. Parents book their lockers online using our secure portal, and we collect their payments.
                        </p>
                        <p class="ProximaNova-Black about2-col-content-text2">
                            The parents are happy, the school is delighted, the students’ kit is secure and everybody wins!
                        </p>
                        <h4 class="ProximaNova-Black border">REQUEST A FREE BROCHURE</h4>
                    </div>
                    <div class="col-md-1"></div>
                </div>
            </div>
        </div>
        <div class="col-md-6 about2-col-2"></div>
    </div>
</section>

<section id="role">
    <div class="container" id="role" data-aos="fade-up">
        <div class="row">
            <div class="col-md-6 video-box align-self-baseline text-left" data-aos="fade-right" data-aos-delay="100">
                <span class="ProximaNova-Black" style="font-size: 14px">YOUR ROLE?</span>
                <h3 class="ProximaNova-Black blue" style="font-size: 26px">THE ROLE OF A FRANCHISEE FALLS INTO THREE SIMPLE PARTS</h3>
                <h6 class="ProximaNova-Regular" style="font-size: 16px">As with all great business concepts, the idea is really simple. The key to a successful business is to first identify a need, and then provide a cost-effective solution. If you can make it work for all parties then you’re onto a winner!</h6>
                <div class="row">
                    <div class="col-md-4 col-sm-4 col-xs-4 text-center">
                        <img src="img/lockers/Handshake.png" class="img-fluid-icon" alt="">
                        <h4 CLASS="ProximaNova-Black blue" style="font-size: 20px">PRESENT</h4>
                    </div>
                    <div class="col-md-4 col-sm-4 col-xs-4 text-center">
                        <img src="img/lockers/4-StageIcons-1.png" class="img-fluid-icon"   alt="">
                        <h4 CLASS="ProximaNova-Black blue" style="font-size: 20px">MANAGE</h4>
                    </div>
                    <div class="col-md-4 col-sm-4 col-xs-4 text-center">
                        <img src="img/lockers/Locker.png" class="img-fluid-icon"   alt="">
                        <h4 CLASS="ProximaNova-Black blue" style="font-size: 20px">MAINTAIN</h4>
                    </div>
                </div>
            </div>
            <div class="col-md-6 pt-3 pt-lg-0 content role-content" data-aos="fade-left" data-aos-delay="100">
                <div class="row">
                    <div class="row">
                        <div class="col-lg-1">
                            <span class="text-center circle">1</span>
                        </div>
                        <div class="col-lg-10">
                            <span class="ProximaNova-Regular" style="font-size: 16px"><b class="blue">Present the concept to the school:</b> surveying the space available and advising on the best configuration of the installation.</span>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-1">
                            <span class="text-center circle">2</span>
                        </div>
                        <div class="col-lg-10">
                            <span class="ProximaNova-Regular" style="font-size: 16px"><b class="blue">Manage the installation:</b> our team will deliver the lockers direct to the school, and you will oversee the installation and ensure the lockers are all set up correctly.</span>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-1">
                            <span class="text-center circle">3</span>
                        </div>
                        <div class="col-lg-10">
                        <span class="ProximaNova-Regular" style="font-size: 16px"><b class="blue">Maintain the lockers: </b> Most of the maintenance
                            comes at the end of the summer term, where lockers are cleaned and codes reset ready for the new intake of
                            students.Typically there will be the occasional ad-hoc maintenance calls to replace a damaged lock or reset
                            a code but these are minimal and most enquiries are handled by the Secure Locker Rentals Head Office team
                        </span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section id="financial">
    <div class="container" data-aos="fade-up">
        <div class="row content">
            <div class="col-md-4 text-right">
                <h1 class="ProximaNova-Black red" style="font-size: 26px">
                    WHAT IT COSTS.
                </h1>
                <h6 class="ProximaNova-Regular" style="font-size: 16px">As a Secure Locker Rentals Franchisee you will be awarded an <b class="ProximaNova-Bold">exclusive
                        territory</b>, and given all of the training, tools and equipment you need to get your business up and running for just </h6>

                <h1 class="ProximaNova-Black red" style="font-size: 22px">
                    £19,950 + VAT.
                </h1>
                <h6 class="ProximaNova-Regular" style="font-size: 11px">
                    (VAT can be reclaimed – contact us for more information).
                </h6>
            </div>
            <div class="col-md-4">
                <div class="topmminus"><img src="img/lockers/shutterstock_1160752531.png" class="img-financial" alt="">
                </div>

            </div>
            <div class="col-md-4 text-left earn-content">


                <h1 class="ProximaNova-Black red" style="font-size: 26px">
                    WHAT YOU CAN EARN.
                </h1>
                <h6 class="ProximaNova-Regular"  style="font-size: 16px">A well-run territory with just 10 schools supplied can generate in excess of
                    <b class="red">£40,000</b> per year after all costs, and remember, certain territories already have schools signed-up which you will take over upon joining so you could reach those earnings levels much quicker</h6>

                <h6 class="ProximaNova-Regular" style="font-size: 11px">
                    (Note: Figures are not guaranteed and actual performance can vary by area, please contact us for more information
                </h6>
            </div>
        </div>

    </div>
</section><!-- End Cost Section -->

<section id="brochure">
    <div class="container" data-aos="fade-up">

        <div class="row">
            <div class="col-md-8 video-box align-self-baseline" data-aos="fade-right" data-aos-delay="100">
                <h2 class="ProximaNova-Black gray" style="font-size: 26px">READY TO FIND OUT MORE?</h2>
                <h6 class="ProximaNova-Regular" style="font-size: 16px">Simply <b class="gray">request a brochure</b> and we’ll get a copy of our latest prospectus on its way to you. This will take you through the business in much more detail, and will help you decide if this is the right business for you (and if you’re the right franchisee for us!).</h6>

            </div>
            <div class="col-md-4 pt-3 pt-lg-0 conten text-centert" data-aos="fade-left" data-aos-delay="100">
                <div class="text-center"style="padding-top: 20px">
                    <button type="button" class="btn btn-info text-center gray button" style=" " data-toggle="modal" data-target="#brochure-modal">REQUEST A FREE BROCHURE</button>
                </div>
            </div>
        </div>

    </div>
    <div class="modal fade" id="brochure-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="text-center" style="background-color: rgb(179, 5, 16)">
                    <p class="Gotham-Black" id="exampleModalLabel" style="font-size: 16px;color: white;padding-top: 2%"> REQUEST A FREE BROCHURE </p>

                </div>
                <div class="modal-body">
                    <form id="request_brochure_form" action="{{route('brochureForm')}}" method="post">
                        <input type="hidden" name="_token" class="csrf-token"  value="{{ Session::token() }}" />

                        <div class="form-group" style="margin-bottom: 0px">
                            <div class="row">
                                <div class="col-md-3 text-right" style="padding-right: 0px" >
                                    <label for="name" class="ProximaNova-Regular text-right" >Name</label>
                                </div>

                                <div class="col-md-9 text-left">
                                    <input type="text" class="ProximaNova-Regular" id="name" name="name" style="width: 100%;font-size: 85%">
                                </div>
                            </div>
                        </div>
                        <div class="form-group" style="margin-bottom: 0px">
                            <div class="row">
                                <div class="col-md-3 text-right" style="padding-right: 0px" >
                                    <label for="address" class="ProximaNova-Regular text-right" >Address</label>
                                </div>

                                <div class="col-md-9 text-left">
                                    <input type="text" class="ProximaNova-Regular" id="address" name="address" style="width: 100%;font-size: 85%">
                                </div>
                            </div>
                        </div>
                        <div class="form-group" style="margin-bottom: 0px">
                            <div class="row">
                                <div class="col-md-3 text-right" style="padding-right: 0px" >
                                    <label for="town" class="ProximaNova-Regular text-right" >Town</label>
                                </div>

                                <div class="col-md-9 text-left">
                                    <input type="text" class="ProximaNova-Regular" id="town" name="town" style="width: 100%;font-size: 85%">
                                </div>
                            </div>
                        </div>
                        <div class="form-group" style="margin-bottom: 0px">
                            <div class="row">
                                <div class="col-md-3 text-right" style="padding-right: 0px" >
                                    <label for="postcode" class="ProximaNova-Regular text-right" >Postcode</label>
                                </div>

                                <div class="col-md-9 text-left">
                                    <input type="text" class="ProximaNova-Regular" id="postcode" name="postcode" style="width: 100%;font-size: 85%">
                                </div>
                            </div>
                        </div>
                        <div class="form-group" style="margin-bottom: 0px">
                            <div class="row">
                                <div class="col-md-3 text-right" style="padding-right: 0px" >
                                    <label for="telephone" class="ProximaNova-Regular text-right" >Telephone</label>
                                </div>

                                <div class="col-md-9 text-left">
                                    <input type="text" class="ProximaNova-Regular" id="telephone" name="telephone" style="width: 100%;font-size: 85%">
                                </div>
                            </div>
                        </div>
                        <div class="form-group" style="margin-bottom: 0px">
                            <div class="row">
                                <div class="col-md-3 text-right" style="padding-right: 0px" >
                                    <label for="mobile" class="ProximaNova-Regular text-right" >Mobile</label>
                                </div>

                                <div class="col-md-9 text-left">
                                    <input type="text" class="ProximaNova-Regular" id="mobile" name="mobile" style="width: 100%;font-size: 85%">
                                </div>
                            </div>
                        </div>
                        <div class="form-group" style="margin-bottom: 0px">
                            <div class="row">
                                <div class="col-md-3 text-right" style="padding-right: 0px" >
                                    <label for="email" class="ProximaNova-Regular text-right" >Email</label>
                                </div>

                                <div class="col-md-9 text-left">
                                    <input type="text" class="ProximaNova-Regular" id="email" name="email" style="width: 100%;font-size: 85%">
                                </div>
                            </div>
                        </div>
                        <div class="form-check">
                            <div class="row">
                                <div class="col-md-3 text-right" style="padding-right: 0px" >
                                </div>

                                <div class="col-md-9 text-left" style="padding-left: 0px">
                                    <input class=" " type="checkbox" value="" id="terms-check" style="border:1px solid rgb(179, 5, 16)">
                                    <label class="ProximaNova-Regular" for="defaultCheck1" style="font-size: 10px">
                                        I have read and agree to the privacy policy
                                    </label>
                                    <p  class="ProximaNova-Regular red d-none" id="terms-validation-msg" style="font-size: 10px">kindly check privacy plicy</p>

                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3 text-right" style="padding-right: 0px" >
                            </div>
                            <div class="col-md-9 text-left" style="padding-left: 3%">
                                <div class="" >
                                    <button type="button" class="btn btn-danger red modal-button Gotham-Black" id="brochure-save-btn" style="border-radius: 0px " data-toggle="modal" data-target="#exampleModal">SUBMIT</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>
</section><!-- End Requst a Brochure Section -->

<section id="footer" class="about" style="background-color: rgb(238, 222, 138); background-image: url('img/color-strip.png'); background-size: contain; background-repeat: no-repeat;">
    <div class="container" data-aos="fade-up">
        <div class="row content">
            <div class="col-md-4 text-left">
                <img src="img/lockers/Secure_Logo_RGB_03.png" class="img-fluid" alt=""  >
                <h6 class="ProximaNova-Regular" style="font-size: 12px">14 Stephenson Court, Priory Business Park, Bedford, MK44 3WH
                    Reg No: 6535355
                    VAT No: 936 5596 78 </h6>
            </div>
            <div class="col-md-4"></div>
            <div class="col-md-4 text-right">
                <i class="fa fa-youtube-play fa-2x" style="font-size:30px;" aria-hidden="true"></i>
                <i class="fa fa-instagram fa-2x" style="font-size:30px;" aria-hidden="true"></i>
                <i class="fa fa-facebook-square fa-2x" aria-hidden="true" style="font-size:27px;"></i>
            </div>
        </div>
    </div>
</section><!-- End Cost Section -->
<script >

    $('#brochure-modal').on('show.bs.modal', function (e) {
        var button = e.relatedTarget;
        if($(button).hasClass('no-modal')) {
            e.stopPropagation();
        }
    });

    $('body').on('click', '#brochure-save-btn', function (e) {

        $('#terms-validation-msg').addClass('d-none');


        if($('#terms-check').prop("checked") == true){
            // alert('chkced');
            $('#request_brochure_form').submit();
        }
        else  {
            // alert('notchkced');
            $('#terms-validation-msg').removeClass('d-none');
            console.log("Checkbox is unchecked.");
        }


    });

</script>
</body>
</html>
