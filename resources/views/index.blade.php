<!DOCTYPE html>
<html lang="en">
<head>
    <title>Secure Locker Rental</title>
    <link rel="icon" type="image/jpg" href="{{asset('/img/lockers/Favicon.jpg')}}"/>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
    <link href="{{asset('css/custom_style.css')}}" rel="stylesheet">
    <script src="https://kit.fontawesome.com/c167da1aca.js" crossorigin="anonymous"></script>
    <link href="https://fonts.googleapis.com/css2?family=Montserrat&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@400;500;900&display=swap" rel="stylesheet">
    <script async data-uid="16638e145f" src="https://hustling-leader-540.ck.page/16638e145f/index.js"></script>
</head>
<body>

<nav class="navbar navbar-default navbar-fixed-top" id="header">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                {{--                <span class="icon-bar"></span>--}}
                {{--                <span class="icon-bar"></span>--}}
                {{--                <span class="icon-bar"></span>--}}
                <img src="{{asset('/img/lockers/icon-bar.png')}}" alt="" style="width: 40px;height:30px;" class="icon-bar">
            </button>
            <a href="{{url('/')}}" class="navbar-brand navbar-logo" style=""><img src="{{asset('/img/lockers/Secure_Logo_RGB_03.png')}}" alt="" class="img-fluid"></a>
        </div>
        <div class="collapse navbar-collapse" id="myNavbar">
            <ul class="nav navbar-nav">
                <li><a class="nav_font_style nav_font_style_black Montserrat-regular" href="#work">HOW IT WORKS</a></li>
                <li><a class="nav_font_style nav_font_style_black Montserrat-regular" href="#role">YOUR ROLE</a></li>
                <li><a class="nav_font_style nav_font_style_black Montserrat-regular" href="#financial">FINANCIALS</a></li>
                <li><a class="nav_font_style nav_font_style_red Montserrat-regular" href="#brochure">REQUEST A FREE BROCHURE</a></li>
            </ul>
        </div>
    </div>
</nav>
<section id="about">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <br>
                <h6 class="Montserrat-regular main_top_heading">
                    UNLOCK YOUR TRUE POTENTIAL.
                </h6>
                <h1 class="Montserrat-regular main_second_heading">
                    A FRANCHISE CONCEPT UNLIKE ANY OTHER.
                </h1>
                <p class="Montserrat-regular main_top_para">Very few businesses offer you ‘residual income’ (that’s income that keeps coming in year after year with minimal work from you) but this one does!  This business redefines ‘part time’ to mean ‘letting the business run itself’. Sure, there’s some work to do getting things set up, but once you’re up and running this is a franchise that can be run alongside any other business or commitments with very minimal input from you. Perhaps you’re just ready to slow things down and enjoy more leisure time?</p>
                <h6 class="Montserrat-regular main_third_heading">Either way, this could be the perfect fit.</h6>
            </div>
            <div class="col-md-6">
                <br>
                <div class="top-img-fluid-section">
                    <img src="{{asset('/img/lockers/top_main_img.png')}}" class="top-img-fluid" alt="">
                </div>
            </div>
        </div>
    </div>
</section>

<section id="work">
    <div class="container" data-aos="fade-up">
        <div class="row" >
            <div class="col-md-6 video-box" data-aos="fade-right" data-aos-delay="100">
                <h2 class="Montserrat-regular blue work-heading">HOW DOES IT WORK?</h2>
                <p class="Montserrat-regular work-text">As with all great business concepts, the idea is really simple. The key to a successful business is to first identify a need, and then provide a cost-effective solution. If you can make it work for all parties then you’re onto a winner!</p>
            </div>
            <div class="col-md-6  content" data-aos="fade-left" data-aos-delay="100">
                <div class="row text-right work-img-row">
                    <div class="col-md-6 col-sm-6 col-xs-6 img-fluid-icon-col text-center">
                        <ul class="img-fluid-icon-list">
                            <li>
                                <img src="{{asset('/img/lockers/Install@1X.png')}}" class="work-img-fluid-icon" alt="">
                                <span class="Gotham-Black blue work-icon-text" >INSTALL</span>
                            </li>
                            <li>
                                <img src="{{asset('/img/lockers/Rent@1X.png')}}" class="work-img-fluid-icon"   alt="">
                                <span class="Gotham-Black blue work-icon-text" >RENT</span>
                            </li>
                            <li>
                                <img src="{{asset('/img/lockers/Earn@1X.png')}}" class="work-img-fluid-icon"   alt="">
                                <span class="Gotham-Black blue work-icon-text">EARN</span>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>

    </div>
</section>

<section id="about-video2">
    <div class="row about-row about-row-1">
        <div class="col-md-6 about-col-1"></div>
        <div class="col-md-6 about-col-2">
            <div class="content-overlay about-col-2-lay">
                <div class="row about-row">
                    <div class="col-md-1"></div>
                    <div class="col-md-11 about-col-2-content">
                        <h6 class="Montserrat-regular">THE PROBLEM? </h6>
                        <h5 class="Montserrat-regular">HEALTH. SECURITY. BUDGET.</h5>
                        <p class="Montserrat-regular">
                            These days, school children and university students carry a lot of expensive equipment around. From £100+ trainers to iPhones and Laptops. All of this kit is heavy and valuable, so ideally they’d like somewhere secure to put it whilst they’re at school. Parents regularly put pressure on schools to install lockers. They want good sized, high quality lockers with decent locks. The problem is that most schools have very limited budgets and installing potentially over 1,000 high quality lockers (and maintaining them) comes at enormous cost, which they simply can’t afford.
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row about-row about-row-2">
        <div class="col-md-6 about2-col-1">
            <div class="content-lay about2-col-1-lay">
                <div class="row">
                    <div class="col-md-2"></div>
                    <div class="col-md-10 about2-col-content">
                        <h6 class="Montserrat-regular">THE SOLUTION? </h6>
                        <h5 class="Montserrat-regular">SIMPLE. SECURE. FREE. </h5>
                        <p class="Montserrat-regular about2-col-content-text1">
                            At Secure Locker Rentals, we install the lockers free of charge to the school. We then make these lockers available for parents to rent on an annual basis at very low cost. Parents book their lockers online using our secure portal, and we collect their payments.

                            <br> <b>
                                The parents are happy, the school is delighted, the students’ kit is secure and everybody wins!
                            </b>
                        </p>
                        {{--                        <button type="button" class="btn Montserrat-regular border text-center  brochure-btn-text-solution Montserrat-regular" data-toggle="modal" data-target="#brochure-modal">REQUEST A FREE BROCHURE</button>--}}
                        <a data-formkit-toggle="16638e145f" class="btn Montserrat-regular border text-center  brochure-btn-text-solution Montserrat-regular" href="https://hustling-leader-540.ck.page/16638e145f">REQUEST A FREE BROCHURE</a>

                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6 about2-col-2"></div>
    </div>
</section>

<section id="role">
    <div class="container" id="role" data-aos="fade-up">
        <div class="row">
            <div class="col-md-6 video-box align-self-baseline text-left" data-aos="fade-right" data-aos-delay="100">
                <span class="Montserrat-regular role-heading-top">YOUR ROLE?</span>
                <h3 class="Montserrat-regular blue role-heading-second">THE ROLE OF A FRANCHISEE FALLS INTO THREE SIMPLE PARTS.</h3>
                <p class="Montserrat-regular role-content-text">As with all great business concepts, the idea is really simple. The key to a successful business is to first identify a need, and then provide a cost-effective solution. If you can make it work for all parties then you’re onto a winner!</p>
                <div class="row role-icon-row">
                    <div class="col-md-4 col-sm-4 col-xs-4 text-center">
                        <img src="{{asset('/img/lockers/Handshake.png')}}" class="img-fluid-icon" alt="">
                    </div>
                    <div class="col-md-4 col-sm-4 col-xs-4 text-center">
                        <img src="{{asset('/img/lockers/Manage@4X.png')}}" class="img-fluid-icon"   alt="">
                    </div>
                    <div class="col-md-4 col-sm-4 col-xs-4 text-center">
                        <img src="{{asset('/img/lockers/Locker.png')}}" class="img-fluid-icon"   alt="">
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4 col-sm-4 col-xs-4 text-center">
                        <span class="Montserrat-regular blue role-icon-text" >PRESENT</span>
                    </div>
                    <div class="col-md-4 col-sm-4 col-xs-4 text-center">
                        <span class="Montserrat-regular blue role-icon-text">MANAGE</span>
                    </div>
                    <div class="col-md-4 col-sm-4 col-xs-4 text-center">
                        <span class="Montserrat-regular blue role-icon-text">MAINTAIN</span>
                    </div>
                </div>
            </div>
            <div class="col-md-6 pt-3 pt-lg-0 content role-content" data-aos="fade-left" data-aos-delay="100">
                <div class="row">
                    <div class="row">
                        <div class="col-lg-1">
                            <img src="{{asset('/img/lockers/list_icon_1.png')}}" class="list_icon_style"  alt="">
                        </div>
                        <div class="col-lg-10">
                            <p class="Montserrat-regular role-list-text"><span class="blue list_heading">Present the concept to the school:</span> surveying the space available and advising on the best configuration of the installation.</p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-1">
                            <img src="{{asset('/img/lockers/list_icon_2.png')}}" class="list_icon_style"   alt="">
                        </div>
                        <div class="col-lg-10">
                            <p class="Montserrat-regular role-list-text"><span class="blue list_heading">Manage the installation:</span> our team will deliver the lockers direct to the school, and you will oversee the installation and ensure the lockers are all set up correctly.</p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-1">
                            <img src="{{asset('/img/lockers/list_icon_3.png')}}" class="list_icon_style"   alt="">
                        </div>
                        <div class="col-lg-10">
                            <p class="Montserrat-regular role-list-text"><span class="blue list_heading">Maintain the lockers: </span> Most of the maintenance
                                comes at the end of the summer term, where lockers are cleaned and codes reset ready for the new intake of
                                students. Typically there will be the occasional ad-hoc maintenance calls to replace a damaged lock or reset
                                a code but these are minimal and most enquiries are handled by the Secure Locker Rentals Head Office team.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section id="financial">
    <div class="container" data-aos="fade-up">
        <div class="row content">
            <div class="col-md-4 text-right">
                <h1 class="Montserrat-regular red cost-heading">
                    WHAT IT COSTS.
                </h1>
                <p class="Montserrat-regular cost-text">
                    As a Secure Locker Rentals Franchisee you will be awarded an
                    <b class="Montserrat-regular">exclusive territory</b>,
                    and given all of the training and support you need to get your business up and running for just
                    <b class="red role-vat">£19,950 + VAT.</b><br>
                    <small class="cost-text-sm">(VAT can be reclaimed – contact us for more information).</small>
                </p>
            </div>
            <div class="col-md-4 topmminus-col">
                <div class="topmminus"><img src="{{asset('/img/lockers/shutterstock_1160752531.png')}}" class="img-financial" alt="">
                </div>

            </div>
            <div class="col-md-4 text-left earn-content">
                <h1 class="Montserrat-regular earn-content-heading red">
                    WHAT YOU CAN EARN.
                </h1>
                <p class="Montserrat-regular earn-content-text">
                    A well-run territory with just 7 schools supplied can generate in excess of
                    <b class="red role-40">£40,000</b> per year after all costs, and remember, certain territories
                    already have schools signed-up which you will take over upon joining so you could reach
                    those earnings levels much quicker!<br>
                    <small class="role-note">
                        (Note: Figures are not guaranteed and actual performance can vary by area, please contact us for more information)
                    </small>
                </p>

            </div>
        </div>

    </div>
</section><!-- End Cost Section -->

<section id="brochure">
    <div class="container" data-aos="fade-up">
        <div class="row">
            <div class="col-md-8 video-box align-self-baseline" data-aos="fade-right" data-aos-delay="100">
                <h2 class="Montserrat-regular gray brochure-heading" >READY TO FIND OUT MORE?</h2>
                <p class="Montserrat-regular brochure-content" >Simply <b class="gray b">request a brochure</b> and we’ll get a copy of our latest prospectus on its way to you. This will take you through the business in much more detail, and will help you decide if this is the right business for you (and if you’re the right franchisee for us!).</p>
            </div>
            <div class="col-md-4 brochure-btn-col pt-10 pt-lg-5 conten text-left" data-aos="fade-left" data-aos-delay="100">
                <div class="text-left brochure-btn">
                    <a data-formkit-toggle="16638e145f" class="btn text-center gray button brochure-btn-text Montserrat-regular" href="https://hustling-leader-540.ck.page/16638e145f">REQUEST A FREE BROCHURE</a>
                </div>
            </div>
        </div>

    </div>
    <div class="modal fade" id="brochure-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="text-center" style="background-color: rgb(179, 5, 16)">
                    <p class="Gotham-Black" id="exampleModalLabel" style="font-size: 16px;color: white;padding-top: 2%"> REQUEST A FREE BROCHURE </p>
                </div>
                <div class="modal-body">
                    <script src="https://f.convertkit.com/ckjs/ck.5.js"></script>
                    <form action="https://app.convertkit.com/forms/1483545/subscriptions" class="seva-form formkit-form" method="post" data-sv-form="1483545" data-uid="16638e145f" data-format="modal" data-version="5" data-options="{&quot;settings&quot;:{&quot;after_subscribe&quot;:{&quot;action&quot;:&quot;message&quot;,&quot;success_message&quot;:&quot;Success! Now check your email to confirm your subscription.&quot;,&quot;redirect_url&quot;:&quot;&quot;},&quot;analytics&quot;:{&quot;google&quot;:null,&quot;facebook&quot;:null,&quot;segment&quot;:null,&quot;pinterest&quot;:null},&quot;modal&quot;:{&quot;trigger&quot;:&quot;timer&quot;,&quot;scroll_percentage&quot;:null,&quot;timer&quot;:5,&quot;devices&quot;:&quot;all&quot;,&quot;show_once_every&quot;:15},&quot;powered_by&quot;:{&quot;show&quot;:true,&quot;url&quot;:&quot;https://convertkit.com?utm_source=dynamic&amp;utm_medium=referral&amp;utm_campaign=poweredby&amp;utm_content=form&quot;},&quot;recaptcha&quot;:{&quot;enabled&quot;:false},&quot;return_visitor&quot;:{&quot;action&quot;:&quot;show&quot;,&quot;custom_content&quot;:&quot;&quot;},&quot;slide_in&quot;:{&quot;display_in&quot;:&quot;bottom_right&quot;,&quot;trigger&quot;:&quot;timer&quot;,&quot;scroll_percentage&quot;:null,&quot;timer&quot;:5,&quot;devices&quot;:&quot;all&quot;,&quot;show_once_every&quot;:15},&quot;sticky_bar&quot;:{&quot;display_in&quot;:&quot;top&quot;,&quot;trigger&quot;:&quot;timer&quot;,&quot;scroll_percentage&quot;:null,&quot;timer&quot;:5,&quot;devices&quot;:&quot;all&quot;,&quot;show_once_every&quot;:15}},&quot;version&quot;:&quot;5&quot;}" min-width="400 500 600 700 800" style="background-color: rgb(255, 255, 255); border-radius: 6px;"><div data-style="full"><div data-element="column" class="formkit-background" style="background-image: url(&quot;https://embed.filekitcdn.com/e/ccJxtppZCYkfynzgzn4eBz/d9NPSbeqSByW7Lj5FnPphE&quot;);"></div><div data-element="column" class="formkit-column"><div class="formkit-header" data-element="header" style="color: rgb(83, 83, 83); font-size: 28px; font-weight: 700;"><h1>Get our how to guide</h1></div><ul class="formkit-alert formkit-alert-error" data-element="errors" data-group="alert"></ul><div data-element="fields" class="seva-fields formkit-fields"><div class="formkit-field"><input class="formkit-input" aria-label="Your first name" name="fields[first_name]" placeholder="Your first name" type="text" style="color: rgb(139, 139, 139); border-color: rgb(221, 224, 228); font-weight: 400;"></div><div class="formkit-field"><input class="formkit-input" name="email_address" placeholder="Your email address" required="" type="email" style="color: rgb(139, 139, 139); border-color: rgb(221, 224, 228); font-weight: 400;"></div><button data-element="submit" class="formkit-submit formkit-submit" style="color: rgb(255, 255, 255); background-color: rgb(246, 166, 171); border-radius: 3px; font-weight: 700;"><div class="formkit-spinner"><div></div><div></div><div></div></div><span>Send me the guide</span></button></div><div class="formkit-disclaimer" data-element="disclaimer" style="color: rgb(139, 139, 139); font-size: 13px;">We respect your privacy. Unsubscribe at anytime.</div><a href="https://convertkit.com?utm_source=dynamic&amp;utm_medium=referral&amp;utm_campaign=poweredby&amp;utm_content=form" class="formkit-powered-by" data-element="powered-by" target="_blank" rel="noopener noreferrer">Powered By ConvertKit</a></div></div><style>.formkit-form[data-uid="16638e145f"] *{box-sizing:border-box;}.formkit-form[data-uid="16638e145f"]{-webkit-font-smoothing:antialiased;-moz-osx-font-smoothing:grayscale;}.formkit-form[data-uid="16638e145f"] legend{border:none;font-size:inherit;margin-bottom:10px;padding:0;position:relative;display:table;}.formkit-form[data-uid="16638e145f"] fieldset{border:0;padding:0.01em 0 0 0;margin:0;min-width:0;}.formkit-form[data-uid="16638e145f"] body:not(:-moz-handler-blocked) fieldset{display:table-cell;}.formkit-form[data-uid="16638e145f"] h1,.formkit-form[data-uid="16638e145f"] h2,.formkit-form[data-uid="16638e145f"] h3,.formkit-form[data-uid="16638e145f"] h4,.formkit-form[data-uid="16638e145f"] h5,.formkit-form[data-uid="16638e145f"] h6{color:inherit;font-size:inherit;font-weight:inherit;}.formkit-form[data-uid="16638e145f"] p{color:inherit;font-size:inherit;font-weight:inherit;}.formkit-form[data-uid="16638e145f"] ol:not([template-default]),.formkit-form[data-uid="16638e145f"] ul:not([template-default]),.formkit-form[data-uid="16638e145f"] blockquote:not([template-default]){text-align:left;}.formkit-form[data-uid="16638e145f"] p:not([template-default]),.formkit-form[data-uid="16638e145f"] hr:not([template-default]),.formkit-form[data-uid="16638e145f"] blockquote:not([template-default]),.formkit-form[data-uid="16638e145f"] ol:not([template-default]),.formkit-form[data-uid="16638e145f"] ul:not([template-default]){color:inherit;font-style:initial;}.formkit-form[data-uid="16638e145f"][data-format="modal"]{display:none;}.formkit-form[data-uid="16638e145f"][data-format="slide in"]{display:none;}.formkit-form[data-uid="16638e145f"][data-format="sticky bar"]{display:none;}.formkit-sticky-bar .formkit-form[data-uid="16638e145f"][data-format="sticky bar"]{display:block;}.formkit-form[data-uid="16638e145f"] .formkit-input,.formkit-form[data-uid="16638e145f"] .formkit-select,.formkit-form[data-uid="16638e145f"] .formkit-checkboxes{width:100%;}.formkit-form[data-uid="16638e145f"] .formkit-button,.formkit-form[data-uid="16638e145f"] .formkit-submit{border:0;border-radius:5px;color:#ffffff;cursor:pointer;display:inline-block;text-align:center;font-size:15px;font-weight:500;cursor:pointer;margin-bottom:15px;overflow:hidden;padding:0;position:relative;vertical-align:middle;}.formkit-form[data-uid="16638e145f"] .formkit-button:hover,.formkit-form[data-uid="16638e145f"] .formkit-submit:hover,.formkit-form[data-uid="16638e145f"] .formkit-button:focus,.formkit-form[data-uid="16638e145f"] .formkit-submit:focus{outline:none;}.formkit-form[data-uid="16638e145f"] .formkit-button:hover > span,.formkit-form[data-uid="16638e145f"] .formkit-submit:hover > span,.formkit-form[data-uid="16638e145f"] .formkit-button:focus > span,.formkit-form[data-uid="16638e145f"] .formkit-submit:focus > span{background-color:rgba(0,0,0,0.1);}.formkit-form[data-uid="16638e145f"] .formkit-button > span,.formkit-form[data-uid="16638e145f"] .formkit-submit > span{display:block;-webkit-transition:all 300ms ease-in-out;transition:all 300ms ease-in-out;padding:12px 24px;}.formkit-form[data-uid="16638e145f"] .formkit-input{background:#ffffff;font-size:15px;padding:12px;border:1px solid #e3e3e3;-webkit-flex:1 0 auto;-ms-flex:1 0 auto;flex:1 0 auto;line-height:1.4;margin:0;-webkit-transition:border-color ease-out 300ms;transition:border-color ease-out 300ms;}.formkit-form[data-uid="16638e145f"] .formkit-input:focus{outline:none;border-color:#1677be;-webkit-transition:border-color ease 300ms;transition:border-color ease 300ms;}.formkit-form[data-uid="16638e145f"] .formkit-input::-webkit-input-placeholder{color:inherit;opacity:0.8;}.formkit-form[data-uid="16638e145f"] .formkit-input::-moz-placeholder{color:inherit;opacity:0.8;}.formkit-form[data-uid="16638e145f"] .formkit-input:-ms-input-placeholder{color:inherit;opacity:0.8;}.formkit-form[data-uid="16638e145f"] .formkit-input::placeholder{color:inherit;opacity:0.8;}.formkit-form[data-uid="16638e145f"] [data-group="dropdown"]{position:relative;display:inline-block;width:100%;}.formkit-form[data-uid="16638e145f"] [data-group="dropdown"]::before{content:"";top:calc(50% - 2.5px);right:10px;position:absolute;pointer-events:none;border-color:#4f4f4f transparent transparent transparent;border-style:solid;border-width:6px 6px 0 6px;height:0;width:0;z-index:999;}.formkit-form[data-uid="16638e145f"] [data-group="dropdown"] select{height:auto;width:100%;cursor:pointer;color:#333333;line-height:1.4;margin-bottom:0;padding:0 6px;-webkit-appearance:none;-moz-appearance:none;appearance:none;font-size:15px;padding:12px;padding-right:25px;border:1px solid #e3e3e3;background:#ffffff;}.formkit-form[data-uid="16638e145f"] [data-group="dropdown"] select:focus{outline:none;}.formkit-form[data-uid="16638e145f"] [data-group="checkboxes"]{text-align:left;margin:0;}.formkit-form[data-uid="16638e145f"] [data-group="checkboxes"] [data-group="checkbox"]{margin-bottom:10px;}.formkit-form[data-uid="16638e145f"] [data-group="checkboxes"] [data-group="checkbox"] *{cursor:pointer;}.formkit-form[data-uid="16638e145f"] [data-group="checkboxes"] [data-group="checkbox"]:last-of-type{margin-bottom:0;}.formkit-form[data-uid="16638e145f"] [data-group="checkboxes"] [data-group="checkbox"] input[type="checkbox"]{display:none;}.formkit-form[data-uid="16638e145f"] [data-group="checkboxes"] [data-group="checkbox"] input[type="checkbox"] + label::after{content:none;}.formkit-form[data-uid="16638e145f"] [data-group="checkboxes"] [data-group="checkbox"] input[type="checkbox"]:checked + label::after{border-color:#ffffff;content:"";}.formkit-form[data-uid="16638e145f"] [data-group="checkboxes"] [data-group="checkbox"] input[type="checkbox"]:checked + label::before{background:#10bf7a;border-color:#10bf7a;}.formkit-form[data-uid="16638e145f"] [data-group="checkboxes"] [data-group="checkbox"] label{position:relative;display:inline-block;padding-left:28px;}.formkit-form[data-uid="16638e145f"] [data-group="checkboxes"] [data-group="checkbox"] label::before,.formkit-form[data-uid="16638e145f"] [data-group="checkboxes"] [data-group="checkbox"] label::after{position:absolute;content:"";display:inline-block;}.formkit-form[data-uid="16638e145f"] [data-group="checkboxes"] [data-group="checkbox"] label::before{height:16px;width:16px;border:1px solid #e3e3e3;background:#ffffff;left:0px;top:3px;}.formkit-form[data-uid="16638e145f"] [data-group="checkboxes"] [data-group="checkbox"] label::after{height:4px;width:8px;border-left:2px solid #4d4d4d;border-bottom:2px solid #4d4d4d;-webkit-transform:rotate(-45deg);-ms-transform:rotate(-45deg);transform:rotate(-45deg);left:4px;top:8px;}.formkit-form[data-uid="16638e145f"] .formkit-alert{background:#f9fafb;border:1px solid #e3e3e3;border-radius:5px;-webkit-flex:1 0 auto;-ms-flex:1 0 auto;flex:1 0 auto;list-style:none;margin:25px auto;padding:12px;text-align:center;width:100%;}.formkit-form[data-uid="16638e145f"] .formkit-alert:empty{display:none;}.formkit-form[data-uid="16638e145f"] .formkit-alert-success{background:#d3fbeb;border-color:#10bf7a;color:#0c905c;}.formkit-form[data-uid="16638e145f"] .formkit-alert-error{background:#fde8e2;border-color:#f2643b;color:#ea4110;}.formkit-form[data-uid="16638e145f"] .formkit-spinner{display:-webkit-box;display:-webkit-flex;display:-ms-flexbox;display:flex;height:0px;width:0px;margin:0 auto;position:absolute;top:0;left:0;right:0;width:0px;overflow:hidden;text-align:center;-webkit-transition:all 300ms ease-in-out;transition:all 300ms ease-in-out;}.formkit-form[data-uid="16638e145f"] .formkit-spinner > div{margin:auto;width:12px;height:12px;background-color:#fff;opacity:0.3;border-radius:100%;display:inline-block;-webkit-animation:formkit-bouncedelay-formkit-form-data-uid-16638e145f- 1.4s infinite ease-in-out both;animation:formkit-bouncedelay-formkit-form-data-uid-16638e145f- 1.4s infinite ease-in-out both;}.formkit-form[data-uid="16638e145f"] .formkit-spinner > div:nth-child(1){-webkit-animation-delay:-0.32s;animation-delay:-0.32s;}.formkit-form[data-uid="16638e145f"] .formkit-spinner > div:nth-child(2){-webkit-animation-delay:-0.16s;animation-delay:-0.16s;}.formkit-form[data-uid="16638e145f"] .formkit-submit[data-active] .formkit-spinner{opacity:1;height:100%;width:50px;}.formkit-form[data-uid="16638e145f"] .formkit-submit[data-active] .formkit-spinner ~ span{opacity:0;}.formkit-form[data-uid="16638e145f"] .formkit-powered-by[data-active="false"]{opacity:0.35;}@-webkit-keyframes formkit-bouncedelay-formkit-form-data-uid-16638e145f-{0%,80%,100%{-webkit-transform:scale(0);-ms-transform:scale(0);transform:scale(0);}40%{-webkit-transform:scale(1);-ms-transform:scale(1);transform:scale(1);}}@keyframes formkit-bouncedelay-formkit-form-data-uid-16638e145f-{0%,80%,100%{-webkit-transform:scale(0);-ms-transform:scale(0);transform:scale(0);}40%{-webkit-transform:scale(1);-ms-transform:scale(1);transform:scale(1);}}.formkit-form[data-uid="16638e145f"] blockquote{padding:10px 20px;margin:0 0 20px;border-left:5px solid #e1e1e1;} .formkit-form[data-uid="16638e145f"]{box-shadow:0 0px 2px rgba(0,0,0,0.15);max-width:700px;overflow:hidden;}.formkit-form[data-uid="16638e145f"] [data-style="full"]{width:100%;display:block;}.formkit-form[data-uid="16638e145f"] .formkit-background{background-position:center center;background-size:cover;min-height:200px;}.formkit-form[data-uid="16638e145f"] .formkit-column{padding:20px;position:relative;}.formkit-form[data-uid="16638e145f"] .formkit-header{margin-top:0;margin-bottom:20px;}.formkit-form[data-uid="16638e145f"] .formkit-field{margin:0 0 10px 0;}.formkit-form[data-uid="16638e145f"] .formkit-input{width:100%;border-left:none;border-right:none;border-top:none;padding-left:0;padding-right:0;}.formkit-form[data-uid="16638e145f"] .formkit-fields .formkit-submit{margin-top:15px;width:100%;}.formkit-form[data-uid="16638e145f"] .formkit-disclaimer{margin:0 0 15px 0;}.formkit-form[data-uid="16638e145f"] .formkit-disclaimer > p{margin:0;}.formkit-form[data-uid="16638e145f"] .formkit-powered-by{color:#7d7d7d;display:block;font-size:11px;margin-bottom:0;margin-top:20px;text-align:center;}.formkit-form[data-uid="16638e145f"][min-width~="600"] [data-style="full"],.formkit-form[data-uid="16638e145f"][min-width~="700"] [data-style="full"],.formkit-form[data-uid="16638e145f"][min-width~="800"] [data-style="full"]{display:grid;grid-template-columns:repeat(auto-fit,minmax(200px,1fr));}.formkit-form[data-uid="16638e145f"][min-width~="600"] .formkit-column,.formkit-form[data-uid="16638e145f"][min-width~="700"] .formkit-column,.formkit-form[data-uid="16638e145f"][min-width~="800"] .formkit-column{padding:40px;} </style></form>
                </div>

            </div>
        </div>
    </div>
</section><!-- End Requst a Brochure Section -->

<section id="footer" class="about" style="background-color: rgb(238, 222, 138); background-image: url('{{asset('/img/color-strip.png')}}'); background-size: contain; background-repeat: no-repeat;">
    <div class="container" data-aos="fade-up">
        <div class="row content footer-row">
            <div class="col-md-4 text-left">
                <img src="{{asset('/img/lockers/Secure_Logo_RGB_03.png')}}" class="img-fluid" alt=""  >
            </div>
            <div class="col-md-4"></div>
            <div class="col-md-4 text-center footer-icon">
                <p class="Montserrat-regular footer-text" >
                    14 Stephenson Court, Priory Business Park,<br> Bedford, MK44 3WH<br>
                    Reg No: 6535355<br>
                    VAT No: 936 5596 78<br>
                </p>

                {{--                <img src="/img/lockers/youtube-icon.png" class="footer-icon-fluid" alt=""  >--}}
                {{--                <img src="/img/lockers/instgram-icon.png" class="footer-icon-fluid" alt=""  >--}}
                {{--                <img src="/img/lockers/facebook-icon.png" class="footer-icon-fluid facebook-icon" alt=""  >--}}
            </div>
        </div>
    </div>
</section><!-- End Cost Section -->
<script >

    $('#brochure-modal').on('show.bs.modal', function (e) {
        var button = e.relatedTarget;
        if($(button).hasClass('no-modal')) {
            e.stopPropagation();
        }
    });

    $('body').on('click', '#brochure-save-btn', function (e) {

        $('#terms-validation-msg').addClass('d-none');


        if($('#terms-check').prop("checked") == true){
            // alert('chkced');
            $('#request_brochure_form').submit();
        }
        else  {
            // alert('notchkced');
            $('#terms-validation-msg').removeClass('d-none');
            console.log("Checkbox is unchecked.");
        }
    });

</script>
</body>
</html>
