<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/brochure.pdf', 'HomeController@brochure');
Route::get('/', 'HomeController@index')->name('staticMainView');
Route::post('/brochureForm', 'HomeController@store')->name('brochureForm');

//Route::get('/', function () {
//    return view('welcome');
//});
